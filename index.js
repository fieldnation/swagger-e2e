const Swagger = require('swagger-client');
const fs = require('fs');
const yaml = require('js-yaml');
const deref = require('json-schema-deref-sync');

const chai = require('chai');
chai.use(require('chai-ajv-json-schema'));

const mocha = require('mocha');
const describe = mocha.describe;
const it = mocha.it;
const before = mocha.before;
const expect = chai.expect;

function loadSpec(spec){
    return deref(yaml.safeLoad(fs.readFileSync(spec, 'utf8')));
}

function SwaggerE2E({spec, authorizations, skipOperations, testSuiteName}, callback){

    skipOperations = Array.isArray(skipOperations)? skipOperations: [skipOperations];

    callback = callback || function(){};
    testSuiteName = testSuiteName || 'Swagger Spec Test'

    let client, error;

    spec = loadSpec(spec);

    new Swagger({
        spec,
        authorizations,
        usePromise: true,
    })
    .then(cl => {
        client = cl;
        if(skipOperations.indexOf('*') === -1) {
            describe(testSuiteName, () => {
                for(let path of Object.keys(spec.paths)){
                    for (let method of Object.keys(spec.paths[path])){
                        let tags = spec.paths[path][method].tags;
                        let operationId = spec.paths[path][method].operationId;

                        if(skipOperations.indexOf(operationId) > -1){
                            break;
                        }
                        let testName = spec.paths[path][method].summary || operationId;
                        it(testName, () => {
                            let tag = tags? tags[0] : 'default';
                            let operation = client[tag].apis[operationId];

                            let params = operation.parameters.reduce((prev, cur) => {
                                if(cur.default !== undefined){
                                    prev[cur.name] = cur.default;
                                    return prev;
                                }
                            }, {});

                            return client[tag][operationId](params).then(resp => {
                                let responseObject = spec.paths[operation.path][operation.method].responses[resp.status];
                                if(responseObject.schema){
                                    expect(resp.obj).to.be.validWithSchema(responseObject.schema);
                                }
                            });
                        });
                    }
                }
            });
        }
        return callback(client);
    }).catch(err => error = err);

    describe(testSuiteName, () => {
        it('should have a valid Swagger spec', () => {
            expect(error).to.be.undefined;
        });
    });
    return spec;
};

SwaggerE2E.validateSchema = (schema, obj)=>{
    expect(obj).to.be.validWithSchema(schema);
};

SwaggerE2E.SwaggerClient = Swagger;

module.exports = SwaggerE2E;