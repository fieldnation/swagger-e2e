# Integration Testing Using Swagger



## Requirements

* `mocha` Testing framework

## Basic Usage

```js
var jwt = require('jsonwebtoken');
var SwaggerE2E = require('swagger-e2e');

let token = {
    fn_token: new SwaggerE2E.SwaggerClient.ApiKeyAuthorization('fn-token',  jwt.sign({ foo: 'bar' }, process.env.TOKEN_SECRET) , 'header')
};

// Start testing all endpoints
SwaggerE2E({
    spec: './swagger.yaml',
    authorizations: token
});
```


## Running Custom Specs

```js
var jwt = require('jsonwebtoken');
var expect = require('chai').expect;
var SwaggerE2E = require('swagger-e2e');

let validToken = {
    fn_token: new SwaggerE2E.SwaggerClient.ApiKeyAuthorization('fn-token',  jwt.sign({ foo: 'bar' }, process.env.TOKEN_SECRET) , 'header')
};
let invalidToken = {
    fn_token: new SwaggerE2E.SwaggerClient.ApiKeyAuthorization('fn-token', 'foo', 'header')
}

let spec = SwaggerE2E({
    spec: './swagger.yaml',
    authorizations: validToken,
    skipOperations: [
        'getStatusById'
    ]
}, (client)=> {
    describe("Custom Spec Test", () => {
        it('Should throw 401 without authentication',(done) => {
            return client.status.getStatus(null, {
                clientAuthorizations: invalidToken
            })
            .catch(err => {
                expect(err.status).to.equal(401)
                SwaggerE2E.validateSchema(spec.definitions.Error, err.obj)
                done();
            })
        });
    });
});
```

## Project Setup:

1. Add dependencies: `yarn add --dev mocha git+https://bitbucket.org/fieldnation/swagger-e2e.git`
2. Initialize `SwaggerE2E` like the examples above.
3. Test with mocha: `mocha --timeout 10000 ./test/e2e/**/*.js`
4. You can use mocha [Root Level Hooks](https://mochajs.org/#root-level-hooks) to setup any application server, database server etc.

To automatically pick up request parameters from Swagger Spec, use `x-example` property, For example:

```yaml
parameters:
  Email:
    name: body
    description: body
    in: body
    required: true
    schema:
      required:
        - email
      properties:
        email:
          $ref: '#/definitions/Email'
    x-example: |-
      {
          "email": {
            "to": ["test@example.com"],
            "from": "test@example.com",
            "subject": "Test",
            "html": "HTML Body"
          }
      }

```

## API:

### `SwaggerE2E(config, [callback])`

Loads and validates Swagger schema from Yaml specification. Also runs integration tests on all operations.

#### Parameters



| Param  | Type                | Required | Description  |
| ------ | ------------------- | -------- | ------------ |
| `config.spec`  | string | Yes | path to yaml spec |
| `config.authorizations` | ApiKeyAuthorization | No | any security definitions used in swagger, see more at https://github.com/swagger-api/swagger-js     |
| `config.skipOperations`  | string/Array | No |  array of `operationId`s from swagger spec to skip testing, to skip all operations use `*` |
| `config.testSuiteName` | string | No | Test suit name, default 'Swagger Spec Test' |
| `callback`  | function | No | Callback function with arguments of a SwaggerClient instance. You can add more custom test cases inside the callback| optional |

**Returns**: Json Swagger Schema

### `SwaggerE2E.SwaggerClient`:

SwaggerClient Constructor. See more at https://github.com/swagger-api/swagger-js


### `SwaggerE2E.validateSchema(schema, obj)`

Validates `obj` with the `schema` and throws AssertionError if not valid.