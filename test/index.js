const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

const spec = './test/petstore.yaml';
let mocha, SwaggerE2E;

function stubSwaggerE2E(){
    mocha = {
        describe: sinon.stub().yields(),
        it: sinon.stub()
    };

    SwaggerE2E = proxyquire('../', {
        mocha
    });
}

test('SwaggerE2E', t => {
    t.test('mocha test', t => {
        stubSwaggerE2E();
        SwaggerE2E({spec}, client => {
            t.equal(2, mocha.describe.callCount, 'describe should be called twice');
            t.equal(21, mocha.it.callCount, 'it should be called 21 times');
            t.end();
        });

    });
    t.test('skip operation test', t => {
        stubSwaggerE2E();
        SwaggerE2E({spec, skipOperations : [
            'findPetsByStatus'
        ]}, client => {
            t.equal(2, mocha.describe.callCount, 'describe should be called twice');
            t.equal(20, mocha.it.callCount, 'it should be called 20 times');
            t.end();
        });
    });

    t.test('skip all operations', t => {
        stubSwaggerE2E();
        SwaggerE2E({spec, skipOperations : '*'}, client => {
            t.equal(1, mocha.describe.callCount, 'describe should be called once');
            t.equal(1, mocha.it.callCount, 'it should be called once');
            t.end();
        });
    });
    t.end();
})
